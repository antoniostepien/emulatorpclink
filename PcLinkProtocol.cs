﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Windows.Forms;
namespace G4XToolKit
{
    class PcLinkProtocol
    {

        public byte[] Array = null;

        public byte[] GetFrameReadID(byte[] FrameBuffer)
        {
            return CheckFrameReadID(FrameBuffer);
        }

        public void CheckSumCalculate(byte[] FrameBuffer)
        {
            for(int n=0; n<FrameBuffer.Length-1; n++)
            {
                FrameBuffer[FrameBuffer.Length - 1] -= FrameBuffer[n];
            }
        }

        public byte[] GetFrameReadGpio(byte[] FrameBuffer)
        {
            return CheckFrameReadGPIO(FrameBuffer);
        }


        public UInt32 CreateGPIOSValues32bits(CheckedListBox checklist)
        {
            UInt32 value = 0;

            for (int i = 0; i< checklist.Items.Count; i++)
            {
                if (checklist.GetItemCheckState(i) == CheckState.Checked) {
                    value |= (UInt32) (1 << i);
                }
            }

            return value;
        }

        private byte[] CheckFrameReadID(byte[] FrameBuffer)
        {
            

            if (FrameBuffer[0]==0x5A && FrameBuffer.Length>3)
            {
                int size = 0;

                size = FrameBuffer[1];          //Size LSB 
                size += FrameBuffer[2]<<8;      //Size MSB 
                size += 3;                      //(ADDRESS=1)+(Lenght=2)+(CRC=1)
                if (size == FrameBuffer.Length)
                {
                    if (CheckSumFrame(FrameBuffer)==0)
                    {
                        /*To do cast here!!*/

                        //UInt32[] Array = new UInt32[(FrameBuffer.Length-4)/4];
                        Array = new byte[(FrameBuffer.Length - 5)];

                        for (int i = 0; i < Array.Length; i++)
                        {
                            Array[i] = FrameBuffer[i+4];
                        }

                        return Array;

                    }
                    else
                    {
                        Array = null;
                    }
                }
                else
                {
                    Array = null;
                }
            }
            else
            {
                Array = null;
            }

            return Array;
        }


        private byte[] CheckFrameReadGPIO(byte[] FrameBuffer)
        {


            if (FrameBuffer[0] == 0x5A && FrameBuffer.Length > 3)
            {
                int size = 0;

                size = FrameBuffer[1];          //Size LSB 
                size += FrameBuffer[2] << 8;      //Size MSB 
                size += 3;                      //(ADDRESS=1)+(Lenght=2)+(CRC=1)
                if (size == FrameBuffer.Length)
                {
                    if (CheckSumFrame(FrameBuffer) == 0)
                    {
                        /*To do cast here!!*/

                        //UInt32[] Array = new UInt32[(FrameBuffer.Length-4)/4];
                        Array = new byte[(FrameBuffer.Length - 5)];

                        for (int i = 0; i < Array.Length; i++)
                        {
                            Array[i] = FrameBuffer[i + 4];
                        }

                        return Array;

                    }
                    else
                    {
                        Array = null;
                    }
                }
                else
                {
                    Array = null;
                }
            }
            else
            {
                Array = null;
            }

            return Array;
        }


        private byte CheckSumFrame(byte[] FrameBuffer)
        {
            byte checksum = 0;

            for (int i = 0; i < (FrameBuffer.Length); i++)
            {
                checksum -= FrameBuffer[i];
            }

            return checksum;
        }


        
    }

   
}
