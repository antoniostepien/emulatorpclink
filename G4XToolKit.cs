﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.Reflection;
using System.Threading;
using System.IO;
using G4XToolKit;

namespace WindowsFormsApplication1
{
    public partial class G4XToolKit : Form 
    {

        PcLinkProtocol cPcLinkProtocol = new PcLinkProtocol();
        
        public G4XToolKit()
        {
            InitializeComponent();
            
            cBPortCOMM.Items.Clear();

            foreach (string commport in System.IO.Ports.SerialPort.GetPortNames())
            {
                //store the each retrieved available port names into the combobox...
                cBPortCOMM.Items.Add(commport);
            }
            /*It Initialize  Serial Port*/
            cBPortCOMM.SelectedIndex = 0;
            cBBaudrate.SelectedIndex = 0;
            /*It Initialize  Serial Port*/
            this.dgTables.ColumnCount = (int)this.nUDSizeX.Value;
            this.dgTables.RowCount = (int)this.nUDSizeY.Value;
            /*disable buttons to avoid send something by serial*/
            this.btRTValuesRead.Enabled = false;
            this.btAnalogRead.Enabled = false;
            this.btTables.Enabled = false;
            this.btGPIOs.Enabled = false;

            


        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (btGPIOs.Text.Equals("Read"))
            {
                btGPIOs.Text = "Reading";
                serialPort1.DataReceived += new SerialDataReceivedEventHandler(button2_DataReceived);
                Thread t = new Thread(button2_WriteThread);
                t.Start();

            }
            else
            {
                btGPIOs.Text = "Read";
                serialPort1.DataReceived -= new SerialDataReceivedEventHandler(button2_DataReceived);
            }

        }

        void button2_WriteThread()
        {

            byte[] Command = { 0x5a, 0x02, 0x10, 0x00 };
            cPcLinkProtocol.CheckSumCalculate(Command);
            Thread.Sleep((int)nUDDelayMS.Value);
            serialPort1.Write(Command, 0, Command.Length);


        }

        private void button2_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(100);
            int count = serialPort1.BytesToRead;
            byte[] ByteArray = new byte[count];
            serialPort1.Read(ByteArray, 0, count);
            

            try
            {
                cPcLinkProtocol.GetFrameReadGpio(ByteArray);

                if (cPcLinkProtocol.Array != null)
                {
                    
                    int Pin32 = 0;
                    Pin32 = cPcLinkProtocol.Array[0];
                    Pin32 += cPcLinkProtocol.Array[1]<<8;
                    Pin32 += cPcLinkProtocol.Array[2]<<16;
                    Pin32 += cPcLinkProtocol.Array[3]<<24;

                    for (int i = 0; i < checkBoxGPIO0.Items.Count;i++)
                    {
                        if ((Pin32 & 0x0001)==1)
                        {
                            checkBoxGPIO0.Invoke((MethodInvoker)(() => checkBoxGPIO0.SetItemChecked(i, true)));
                        }
                        else
                        {
                            checkBoxGPIO0.Invoke((MethodInvoker)(() => checkBoxGPIO0.SetItemChecked(i, false)));
                        }
                        Pin32 = Pin32 >> 1;
                    }

                    /*
                    Pin32 = pclink.Array[4];
                    Pin32 += pclink.Array[5];
                    Pin32 += pclink.Array[6];
                    Pin32 += pclink.Array[7];

                    for (int i = 0; i < checkBoxGPIO1.Items.Count;)
                    {
                        if ((Pin32 & 0x0001) == 1)
                        {
                            checkBoxGPIO1.Invoke((MethodInvoker)(() => checkBoxGPIO1.SetItemChecked(i, true)));
                        }
                        else
                        {
                            checkBoxGPIO1.Invoke((MethodInvoker)(() => checkBoxGPIO1.SetItemChecked(i, false)));
                        }
                        Pin32 = Pin32 >> 1;
                    }
                    */
                }

                if (nUDRepeatRTValue.Value >= 2)
                {
                    nUDRepeatRTValue.Invoke((MethodInvoker)(() => nUDRepeatRTValue.Value--));
                    Thread t = new Thread(button5WriteThread);
                    t.Start();
                }
                else
                {
                    /*It has got to do because of the thread is running*/
                    btRTValuesRead.Invoke((MethodInvoker)(() => btRTValuesRead.Text = "Read"));
                    serialPort1.DataReceived -= new SerialDataReceivedEventHandler(button5_DataReceived);
                }
            }
            catch
            {
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        void button3_WriteThread()
        {

            byte[] Command = { 0x5a, 0x02, 0x0a, 0x00 };
            cPcLinkProtocol.CheckSumCalculate(Command);
            Thread.Sleep((int)nUDDelayMS.Value);
            serialPort1.Write(Command, 0, Command.Length);


        }

        private void button3_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(100);
            int count = serialPort1.BytesToRead;
            byte[] ByteArray = new byte[count];
            serialPort1.Read(ByteArray, 0, count);
            

            try
            {
                cPcLinkProtocol.GetFrameReadID(ByteArray);

                if (cPcLinkProtocol.Array != null)
                {
                    /*It has got to do because of the thread is running*/
                    dgFiledRTValueID.Invoke((MethodInvoker)(() => dgFiledRTValueID.RowCount = cPcLinkProtocol.Array.Length));

                    for (int n = 0; (n < dgFiledRTValueID.RowCount && n < cPcLinkProtocol.Array.Length); n++)
                    {
                        dgFiledRTValueID[0, n].Value = cPcLinkProtocol.Array[n];
                    }

                }

                if (nUDRepeatRTValue.Value >= 2)
                {
                    nUDRepeatRTValue.Invoke((MethodInvoker)(() => nUDRepeatRTValue.Value--));
                    Thread t = new Thread(button5WriteThread);
                    t.Start();
                }
                else
                {
                    /*It has got to do because of the thread is running*/
                    btRTValuesRead.Invoke((MethodInvoker)(() => btRTValuesRead.Text = "Read"));
                    serialPort1.DataReceived -= new SerialDataReceivedEventHandler(button5_DataReceived);
                }
            }
            catch
            {
            }
        }

        private void treeView2_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
         
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void G4XToolKit_Load(object sender, EventArgs e)
        {
       
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            dgTables.ColumnCount = (int) nUDSizeX.Value;
            
        }
        
        private void nUDSizeY_ValueChanged(object sender, EventArgs e)
        {
            dgTables.RowCount = (int)nUDSizeY.Value;
        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void cBPortCOMM_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (this.btConnectSerial.Text.Equals("Disconnect"))
            {
                this.serialPort1.Close();
                this.btConnectSerial.Enabled = true;
                /*Disable all button as related */
                this.btConnectSerial.Text = "Connect";
                this.btRTValuesRead.Enabled = false;
                this.btAnalogRead.Enabled = false;
                this.btTables.Enabled = false;
                this.btGPIOs.Enabled = false;
            }
            else
            {
                
                if(this.serialPort1.PortName.Equals(cBPortCOMM.Text)==false)
                {
                    this.serialPort1.PortName = cBPortCOMM.Text.ToString();
                }
                serialPort1.ReadTimeout = 1000;
                this.serialPort1.Open(); //opens the port
                if (this.serialPort1.IsOpen)
                {
                    this.btConnectSerial.Enabled = true;
                    this.btConnectSerial.Text = "Disconnect";
                    /*Enable all button as related */
                    this.btRTValuesRead.Enabled = true;
                    this.btAnalogRead.Enabled = true;
                    this.btTables.Enabled = true;
                    this.btGPIOs.Enabled = true;
                }
                

            }

        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void rbWrite_CheckedChanged(object sender, EventArgs e)
        {
                       

        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (btRTValuesRead.Text.Equals("Read"))
            {
                btRTValuesRead.Text = "Reading";
                serialPort1.DataReceived += new SerialDataReceivedEventHandler(button5_DataReceived);
                Thread t = new Thread(button5WriteThread);
                t.Start();
                
            }
            else
            {
                btRTValuesRead.Text = "Read";
                serialPort1.DataReceived -= new SerialDataReceivedEventHandler(button5_DataReceived);
            }

        }
        
        void button5WriteThread()
        {
            
            byte[] Command = { 0x5a, 0x02, 0x0a, 0x00 };
            cPcLinkProtocol.CheckSumCalculate(Command);
            Thread.Sleep((int)nUDDelayMS.Value);
            serialPort1.Write(Command, 0, Command.Length);
            
            
        }

        private void button5_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            Thread.Sleep(100);
            int count = serialPort1.BytesToRead;
            byte[] ByteArray = new byte[count];
            serialPort1.Read(ByteArray, 0, count);
            PcLinkProtocol pclink = new PcLinkProtocol();

            try
            {
                pclink.GetFrameReadID(ByteArray);
          
                if (pclink.Array != null)
                {
                    /*It has got to do because of the thread is running*/
                    dgFiledRTValueID.Invoke((MethodInvoker)(() => dgFiledRTValueID.RowCount = pclink.Array.Length));

                    for (int n = 0; (n < dgFiledRTValueID.RowCount && n < pclink.Array.Length); n++)
                    {
                        dgFiledRTValueID[0, n].Value = pclink.Array[n];
                    }
                
                }

                if (nUDRepeatRTValue.Value>=2)
                {
                    nUDRepeatRTValue.Invoke((MethodInvoker)(() => nUDRepeatRTValue.Value--));
                    Thread t = new Thread(button5WriteThread);
                    t.Start();  
                }
                else
                {
                    /*It has got to do because of the thread is running*/
                    btRTValuesRead.Invoke((MethodInvoker)(() => btRTValuesRead.Text = "Read"));
                    serialPort1.DataReceived -= new SerialDataReceivedEventHandler(button5_DataReceived);
                }
            }
            catch
            {
            }
        }

        private void RTValues_Click(object sender, EventArgs e)
        {
            if (this.serialPort1.IsOpen)
            {
                this.btRTValuesRead.Enabled = true;
                
            }
        }

        private void dgFiledRTValueID_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void nUDFields_ValueChanged(object sender, EventArgs e)
        {
            
        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (btWriteGpios.Text.Equals("Write Gpios"))
            {
                btWriteGpios.Text = "Writing...";
               // serialPort1.DataReceived += new SerialDataReceivedEventHandler(button1_DataReceived);
                Thread t = new Thread(button1WriteThread);
                t.Start();

            }
            else
            {
                btWriteGpios.Text = "Write Gpios";
               // serialPort1.DataReceived -= new SerialDataReceivedEventHandler(button1_DataReceived);
            }

        }

        void button1WriteThread()
        {

            byte[] Command = new byte[(checkBoxGPIO1.Items.Count/8)+4];
            UInt32 value32 = cPcLinkProtocol.CreateGPIOSValues32bits(checkBoxGPIO1);

            byte[] byteArray = BitConverter.GetBytes(value32);
            Command[0] = 0x5A;
            Command[1] = (byte)(Command.Length - 1);
            Command[2] = 0x11;
            byteArray.CopyTo(Command, 3);

            cPcLinkProtocol.CheckSumCalculate(Command);
            serialPort1.Write(Command, 0, Command.Length);
            btWriteGpios.Invoke((MethodInvoker)(() => btWriteGpios.Text = "Write Gpios"));

        }

    }
}
