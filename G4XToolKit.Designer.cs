﻿namespace WindowsFormsApplication1
{
    partial class G4XToolKit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(G4XToolKit));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.TreeNode treeNode8 = new System.Windows.Forms.TreeNode("Field00");
            System.Windows.Forms.TreeNode treeNode9 = new System.Windows.Forms.TreeNode("Field01");
            System.Windows.Forms.TreeNode treeNode10 = new System.Windows.Forms.TreeNode("Field02");
            System.Windows.Forms.TreeNode treeNode11 = new System.Windows.Forms.TreeNode("Field03");
            System.Windows.Forms.TreeNode treeNode12 = new System.Windows.Forms.TreeNode("Field04");
            System.Windows.Forms.TreeNode treeNode13 = new System.Windows.Forms.TreeNode("Field05");
            System.Windows.Forms.TreeNode treeNode14 = new System.Windows.Forms.TreeNode("Frame", new System.Windows.Forms.TreeNode[] {
            treeNode8,
            treeNode9,
            treeNode10,
            treeNode11,
            treeNode12,
            treeNode13});
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabG4X = new System.Windows.Forms.TabPage();
            this.btGetConfig = new System.Windows.Forms.Button();
            this.ckBoxChecksum = new System.Windows.Forms.CheckBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.cBBaudrate = new System.Windows.Forms.ComboBox();
            this.cBPortCOMM = new System.Windows.Forms.ComboBox();
            this.btConnectSerial = new System.Windows.Forms.Button();
            this.RTValues = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.nUDDelayMS = new System.Windows.Forms.NumericUpDown();
            this.lRTValueRepeat = new System.Windows.Forms.Label();
            this.nUDRepeatRTValue = new System.Windows.Forms.NumericUpDown();
            this.dgFiledRTValueID = new System.Windows.Forms.DataGridView();
            this.RTValueCol1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.RTValue = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btRTValuesRead = new System.Windows.Forms.Button();
            this.Tables = new System.Windows.Forms.TabPage();
            this.rbRead = new System.Windows.Forms.RadioButton();
            this.rbWrite = new System.Windows.Forms.RadioButton();
            this.label5 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.nUDSizeY = new System.Windows.Forms.NumericUpDown();
            this.nUDSizeX = new System.Windows.Forms.NumericUpDown();
            this.dgTables = new System.Windows.Forms.DataGridView();
            this.btTables = new System.Windows.Forms.Button();
            this.GPIOs = new System.Windows.Forms.TabPage();
            this.checkBoxGPIO1 = new System.Windows.Forms.CheckedListBox();
            this.btWriteGpios = new System.Windows.Forms.Button();
            this.btGPIOs = new System.Windows.Forms.Button();
            this.checkBoxGPIO0 = new System.Windows.Forms.CheckedListBox();
            this.AnalogsInputs = new System.Windows.Forms.TabPage();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.btAnalogRead = new System.Windows.Forms.Button();
            this.treeView2 = new System.Windows.Forms.TreeView();
            this.g4XToolKitBindingSource1 = new System.Windows.Forms.BindingSource(this.components);
            this.programBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.g4XToolKitBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.tabControl1.SuspendLayout();
            this.tabG4X.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.RTValues.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDDelayMS)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDRepeatRTValue)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFiledRTValueID)).BeginInit();
            this.Tables.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSizeY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSizeX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTables)).BeginInit();
            this.GPIOs.SuspendLayout();
            this.AnalogsInputs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.g4XToolKitBindingSource1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.programBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.g4XToolKitBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // serialPort1
            // 
            this.serialPort1.BaudRate = 115200;
            this.serialPort1.ReadBufferSize = 63536;
            this.serialPort1.WriteTimeout = 10000;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabG4X);
            this.tabControl1.Controls.Add(this.RTValues);
            this.tabControl1.Controls.Add(this.Tables);
            this.tabControl1.Controls.Add(this.GPIOs);
            this.tabControl1.Controls.Add(this.AnalogsInputs);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(704, 574);
            this.tabControl1.TabIndex = 0;
            // 
            // tabG4X
            // 
            this.tabG4X.Controls.Add(this.btGetConfig);
            this.tabG4X.Controls.Add(this.ckBoxChecksum);
            this.tabG4X.Controls.Add(this.pictureBox1);
            this.tabG4X.Controls.Add(this.label2);
            this.tabG4X.Controls.Add(this.label1);
            this.tabG4X.Controls.Add(this.cBBaudrate);
            this.tabG4X.Controls.Add(this.cBPortCOMM);
            this.tabG4X.Controls.Add(this.btConnectSerial);
            this.tabG4X.Location = new System.Drawing.Point(4, 22);
            this.tabG4X.Name = "tabG4X";
            this.tabG4X.Padding = new System.Windows.Forms.Padding(3);
            this.tabG4X.Size = new System.Drawing.Size(696, 548);
            this.tabG4X.TabIndex = 0;
            this.tabG4X.Text = "Communication";
            this.tabG4X.UseVisualStyleBackColor = true;
            // 
            // btGetConfig
            // 
            this.btGetConfig.Location = new System.Drawing.Point(380, 47);
            this.btGetConfig.Name = "btGetConfig";
            this.btGetConfig.Size = new System.Drawing.Size(299, 47);
            this.btGetConfig.TabIndex = 7;
            this.btGetConfig.Text = "Get Config";
            this.btGetConfig.UseVisualStyleBackColor = true;
            // 
            // ckBoxChecksum
            // 
            this.ckBoxChecksum.AutoSize = true;
            this.ckBoxChecksum.Checked = true;
            this.ckBoxChecksum.CheckState = System.Windows.Forms.CheckState.Checked;
            this.ckBoxChecksum.Location = new System.Drawing.Point(10, 59);
            this.ckBoxChecksum.Name = "ckBoxChecksum";
            this.ckBoxChecksum.Size = new System.Drawing.Size(76, 17);
            this.ckBoxChecksum.TabIndex = 6;
            this.ckBoxChecksum.Text = "Checksum";
            this.ckBoxChecksum.UseVisualStyleBackColor = true;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox1.ErrorImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.ErrorImage")));
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(215, 248);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(240, 120);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(299, 25);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Baudrate";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(7, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(62, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "COMM Port";
            // 
            // cBBaudrate
            // 
            this.cBBaudrate.FormattingEnabled = true;
            this.cBBaudrate.Items.AddRange(new object[] {
            "115200",
            "230400",
            "460800",
            "921600"});
            this.cBBaudrate.Location = new System.Drawing.Point(380, 20);
            this.cBBaudrate.Name = "cBBaudrate";
            this.cBBaudrate.Size = new System.Drawing.Size(202, 21);
            this.cBBaudrate.TabIndex = 2;
            // 
            // cBPortCOMM
            // 
            this.cBPortCOMM.FormattingEnabled = true;
            this.cBPortCOMM.Items.AddRange(new object[] {
            "COM1",
            "COM2",
            "COM3",
            "COM4",
            "COM5",
            "COM6",
            "COM7",
            "COM8",
            "COM9",
            "COM10",
            "COM11",
            "COM12",
            "COM13",
            "COM14",
            "COM15"});
            this.cBPortCOMM.Location = new System.Drawing.Point(75, 20);
            this.cBPortCOMM.Name = "cBPortCOMM";
            this.cBPortCOMM.Size = new System.Drawing.Size(202, 21);
            this.cBPortCOMM.TabIndex = 1;
            this.cBPortCOMM.SelectedIndexChanged += new System.EventHandler(this.cBPortCOMM_SelectedIndexChanged);
            // 
            // btConnectSerial
            // 
            this.btConnectSerial.Location = new System.Drawing.Point(604, 20);
            this.btConnectSerial.Name = "btConnectSerial";
            this.btConnectSerial.Size = new System.Drawing.Size(75, 23);
            this.btConnectSerial.TabIndex = 0;
            this.btConnectSerial.Text = "Connect";
            this.btConnectSerial.UseVisualStyleBackColor = true;
            this.btConnectSerial.Click += new System.EventHandler(this.button1_Click);
            // 
            // RTValues
            // 
            this.RTValues.Controls.Add(this.label6);
            this.RTValues.Controls.Add(this.nUDDelayMS);
            this.RTValues.Controls.Add(this.lRTValueRepeat);
            this.RTValues.Controls.Add(this.nUDRepeatRTValue);
            this.RTValues.Controls.Add(this.dgFiledRTValueID);
            this.RTValues.Controls.Add(this.btRTValuesRead);
            this.RTValues.Location = new System.Drawing.Point(4, 22);
            this.RTValues.Name = "RTValues";
            this.RTValues.Padding = new System.Windows.Forms.Padding(3);
            this.RTValues.Size = new System.Drawing.Size(696, 548);
            this.RTValues.TabIndex = 1;
            this.RTValues.Text = "RTValues";
            this.RTValues.UseVisualStyleBackColor = true;
            this.RTValues.Click += new System.EventHandler(this.RTValues_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(158, 76);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(149, 13);
            this.label6.TabIndex = 7;
            this.label6.Text = "Delay between each msg (ms)";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // nUDDelayMS
            // 
            this.nUDDelayMS.Location = new System.Drawing.Point(161, 92);
            this.nUDDelayMS.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.nUDDelayMS.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDDelayMS.Name = "nUDDelayMS";
            this.nUDDelayMS.Size = new System.Drawing.Size(136, 20);
            this.nUDDelayMS.TabIndex = 6;
            this.nUDDelayMS.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDDelayMS.ValueChanged += new System.EventHandler(this.nUDFields_ValueChanged);
            // 
            // lRTValueRepeat
            // 
            this.lRTValueRepeat.AutoSize = true;
            this.lRTValueRepeat.Location = new System.Drawing.Point(177, 17);
            this.lRTValueRepeat.Name = "lRTValueRepeat";
            this.lRTValueRepeat.Size = new System.Drawing.Size(130, 13);
            this.lRTValueRepeat.TabIndex = 5;
            this.lRTValueRepeat.Text = "Repeat (63535 - is infinite)";
            // 
            // nUDRepeatRTValue
            // 
            this.nUDRepeatRTValue.Location = new System.Drawing.Point(161, 33);
            this.nUDRepeatRTValue.Maximum = new decimal(new int[] {
            63535,
            0,
            0,
            0});
            this.nUDRepeatRTValue.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDRepeatRTValue.Name = "nUDRepeatRTValue";
            this.nUDRepeatRTValue.Size = new System.Drawing.Size(136, 20);
            this.nUDRepeatRTValue.TabIndex = 3;
            this.nUDRepeatRTValue.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // dgFiledRTValueID
            // 
            this.dgFiledRTValueID.AllowUserToOrderColumns = true;
            this.dgFiledRTValueID.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgFiledRTValueID.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.RTValueCol1,
            this.RTValue});
            this.dgFiledRTValueID.Location = new System.Drawing.Point(342, 7);
            this.dgFiledRTValueID.Name = "dgFiledRTValueID";
            this.dgFiledRTValueID.ReadOnly = true;
            this.dgFiledRTValueID.Size = new System.Drawing.Size(346, 361);
            this.dgFiledRTValueID.TabIndex = 2;
            this.dgFiledRTValueID.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgFiledRTValueID_CellContentClick);
            // 
            // RTValueCol1
            // 
            this.RTValueCol1.HeaderText = "RTValueID";
            this.RTValueCol1.Name = "RTValueCol1";
            this.RTValueCol1.ReadOnly = true;
            // 
            // RTValue
            // 
            this.RTValue.HeaderText = "Value";
            this.RTValue.Name = "RTValue";
            this.RTValue.ReadOnly = true;
            // 
            // btRTValuesRead
            // 
            this.btRTValuesRead.Location = new System.Drawing.Point(8, 6);
            this.btRTValuesRead.Name = "btRTValuesRead";
            this.btRTValuesRead.Size = new System.Drawing.Size(100, 106);
            this.btRTValuesRead.TabIndex = 1;
            this.btRTValuesRead.Text = "Read";
            this.btRTValuesRead.UseVisualStyleBackColor = true;
            this.btRTValuesRead.Click += new System.EventHandler(this.button5_Click);
            // 
            // Tables
            // 
            this.Tables.Controls.Add(this.rbRead);
            this.Tables.Controls.Add(this.rbWrite);
            this.Tables.Controls.Add(this.label5);
            this.Tables.Controls.Add(this.numericUpDown1);
            this.Tables.Controls.Add(this.label4);
            this.Tables.Controls.Add(this.label3);
            this.Tables.Controls.Add(this.nUDSizeY);
            this.Tables.Controls.Add(this.nUDSizeX);
            this.Tables.Controls.Add(this.dgTables);
            this.Tables.Controls.Add(this.btTables);
            this.Tables.Location = new System.Drawing.Point(4, 22);
            this.Tables.Name = "Tables";
            this.Tables.Size = new System.Drawing.Size(696, 548);
            this.Tables.TabIndex = 3;
            this.Tables.Text = "Tables";
            this.Tables.UseVisualStyleBackColor = true;
            // 
            // rbRead
            // 
            this.rbRead.AutoSize = true;
            this.rbRead.Checked = true;
            this.rbRead.Location = new System.Drawing.Point(88, 19);
            this.rbRead.Name = "rbRead";
            this.rbRead.Size = new System.Drawing.Size(51, 17);
            this.rbRead.TabIndex = 11;
            this.rbRead.TabStop = true;
            this.rbRead.Text = "Read";
            this.rbRead.UseVisualStyleBackColor = true;
            // 
            // rbWrite
            // 
            this.rbWrite.AutoSize = true;
            this.rbWrite.Location = new System.Drawing.Point(89, 3);
            this.rbWrite.Name = "rbWrite";
            this.rbWrite.Size = new System.Drawing.Size(50, 17);
            this.rbWrite.TabIndex = 10;
            this.rbWrite.Text = "Write";
            this.rbWrite.UseVisualStyleBackColor = true;
            this.rbWrite.CheckedChanged += new System.EventHandler(this.rbWrite_CheckedChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(232, 18);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(48, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Table ID";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(297, 16);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDown1.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 20);
            this.numericUpDown1.TabIndex = 8;
            this.numericUpDown1.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(607, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(37, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Size Y";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(426, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Size X";
            // 
            // nUDSizeY
            // 
            this.nUDSizeY.Location = new System.Drawing.Point(650, 16);
            this.nUDSizeY.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nUDSizeY.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDSizeY.Name = "nUDSizeY";
            this.nUDSizeY.Size = new System.Drawing.Size(120, 20);
            this.nUDSizeY.TabIndex = 5;
            this.nUDSizeY.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDSizeY.ValueChanged += new System.EventHandler(this.nUDSizeY_ValueChanged);
            // 
            // nUDSizeX
            // 
            this.nUDSizeX.Location = new System.Drawing.Point(469, 16);
            this.nUDSizeX.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.nUDSizeX.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDSizeX.Name = "nUDSizeX";
            this.nUDSizeX.Size = new System.Drawing.Size(120, 20);
            this.nUDSizeX.TabIndex = 4;
            this.nUDSizeX.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nUDSizeX.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // dgTables
            // 
            this.dgTables.AllowUserToResizeColumns = false;
            this.dgTables.AllowUserToResizeRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.NullValue = "0";
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgTables.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle4;
            this.dgTables.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.ColumnHeader;
            this.dgTables.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgTables.ClipboardCopyMode = System.Windows.Forms.DataGridViewClipboardCopyMode.EnableAlwaysIncludeHeaderText;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgTables.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgTables.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.NullValue = "0";
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgTables.DefaultCellStyle = dataGridViewCellStyle6;
            this.dgTables.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnEnter;
            this.dgTables.Location = new System.Drawing.Point(3, 43);
            this.dgTables.Name = "dgTables";
            this.dgTables.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgTables.Size = new System.Drawing.Size(685, 326);
            this.dgTables.TabIndex = 0;
            this.dgTables.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // btTables
            // 
            this.btTables.Location = new System.Drawing.Point(7, 13);
            this.btTables.Name = "btTables";
            this.btTables.Size = new System.Drawing.Size(75, 23);
            this.btTables.TabIndex = 0;
            this.btTables.Text = "Send";
            this.btTables.UseVisualStyleBackColor = true;
            this.btTables.Click += new System.EventHandler(this.button3_Click);
            // 
            // GPIOs
            // 
            this.GPIOs.Controls.Add(this.checkBoxGPIO1);
            this.GPIOs.Controls.Add(this.btWriteGpios);
            this.GPIOs.Controls.Add(this.btGPIOs);
            this.GPIOs.Controls.Add(this.checkBoxGPIO0);
            this.GPIOs.Location = new System.Drawing.Point(4, 22);
            this.GPIOs.Name = "GPIOs";
            this.GPIOs.Size = new System.Drawing.Size(696, 548);
            this.GPIOs.TabIndex = 4;
            this.GPIOs.Text = "GPIOs";
            this.GPIOs.UseVisualStyleBackColor = true;
            // 
            // checkBoxGPIO1
            // 
            this.checkBoxGPIO1.CheckOnClick = true;
            this.checkBoxGPIO1.ColumnWidth = 1;
            this.checkBoxGPIO1.FormattingEnabled = true;
            this.checkBoxGPIO1.Items.AddRange(new object[] {
            "GPIO0",
            "GPIO1",
            "GPIO2",
            "GPIO3",
            "GPIO4",
            "GPIO5",
            "GPIO6",
            "GPIO7",
            "GPIO8",
            "GPIO9",
            "GPIO10",
            "GPIO11",
            "GPIO12",
            "GPIO13",
            "GPIO14",
            "GPIO15",
            "GPIO16",
            "GPIO17",
            "GPIO18",
            "GPIO19",
            "GPIO20",
            "GPIO21",
            "GPIO22",
            "GPIO23",
            "GPIO24",
            "GPIO25",
            "GPIO26",
            "GPIO27",
            "GPIO28",
            "GPIO29",
            "GPIO30",
            "GPIO31"});
            this.checkBoxGPIO1.Location = new System.Drawing.Point(170, 61);
            this.checkBoxGPIO1.Name = "checkBoxGPIO1";
            this.checkBoxGPIO1.Size = new System.Drawing.Size(159, 484);
            this.checkBoxGPIO1.TabIndex = 4;
            // 
            // btWriteGpios
            // 
            this.btWriteGpios.Location = new System.Drawing.Point(169, 3);
            this.btWriteGpios.Name = "btWriteGpios";
            this.btWriteGpios.Size = new System.Drawing.Size(160, 40);
            this.btWriteGpios.TabIndex = 3;
            this.btWriteGpios.Text = "Write GPIOs";
            this.btWriteGpios.UseVisualStyleBackColor = true;
            this.btWriteGpios.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // btGPIOs
            // 
            this.btGPIOs.Location = new System.Drawing.Point(3, 3);
            this.btGPIOs.Name = "btGPIOs";
            this.btGPIOs.Size = new System.Drawing.Size(160, 40);
            this.btGPIOs.TabIndex = 2;
            this.btGPIOs.Text = "Read GPIOs";
            this.btGPIOs.UseVisualStyleBackColor = true;
            this.btGPIOs.Click += new System.EventHandler(this.button2_Click);
            // 
            // checkBoxGPIO0
            // 
            this.checkBoxGPIO0.CheckOnClick = true;
            this.checkBoxGPIO0.ColumnWidth = 1;
            this.checkBoxGPIO0.FormattingEnabled = true;
            this.checkBoxGPIO0.Items.AddRange(new object[] {
            "GPIO0",
            "GPIO1",
            "GPIO2",
            "GPIO3",
            "GPIO4",
            "GPIO5",
            "GPIO6",
            "GPIO7",
            "GPIO8",
            "GPIO9",
            "GPIO10",
            "GPIO11",
            "GPIO12",
            "GPIO13",
            "GPIO14",
            "GPIO15",
            "GPIO16",
            "GPIO17",
            "GPIO18",
            "GPIO19",
            "GPIO20",
            "GPIO21",
            "GPIO22",
            "GPIO23",
            "GPIO24",
            "GPIO25",
            "GPIO26",
            "GPIO27",
            "GPIO28",
            "GPIO29",
            "GPIO30",
            "GPIO31"});
            this.checkBoxGPIO0.Location = new System.Drawing.Point(3, 62);
            this.checkBoxGPIO0.Name = "checkBoxGPIO0";
            this.checkBoxGPIO0.Size = new System.Drawing.Size(159, 484);
            this.checkBoxGPIO0.TabIndex = 1;
            // 
            // AnalogsInputs
            // 
            this.AnalogsInputs.Controls.Add(this.checkBox1);
            this.AnalogsInputs.Controls.Add(this.btAnalogRead);
            this.AnalogsInputs.Controls.Add(this.treeView2);
            this.AnalogsInputs.Location = new System.Drawing.Point(4, 22);
            this.AnalogsInputs.Name = "AnalogsInputs";
            this.AnalogsInputs.Size = new System.Drawing.Size(696, 548);
            this.AnalogsInputs.TabIndex = 5;
            this.AnalogsInputs.Text = "Analog Inputs";
            this.AnalogsInputs.UseVisualStyleBackColor = true;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(333, 17);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(109, 17);
            this.checkBox1.TabIndex = 5;
            this.checkBox1.Text = "Continuous Mode";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // btAnalogRead
            // 
            this.btAnalogRead.Location = new System.Drawing.Point(159, 17);
            this.btAnalogRead.Name = "btAnalogRead";
            this.btAnalogRead.Size = new System.Drawing.Size(153, 102);
            this.btAnalogRead.TabIndex = 4;
            this.btAnalogRead.Text = "Read";
            this.btAnalogRead.UseVisualStyleBackColor = true;
            // 
            // treeView2
            // 
            this.treeView2.Location = new System.Drawing.Point(7, 17);
            this.treeView2.Name = "treeView2";
            treeNode8.Name = "Node3";
            treeNode8.Text = "Field00";
            treeNode9.Name = "Node4";
            treeNode9.Text = "Field01";
            treeNode10.Name = "Node5";
            treeNode10.Text = "Field02";
            treeNode11.Name = "Node6";
            treeNode11.Text = "Field03";
            treeNode12.Name = "Node7";
            treeNode12.Text = "Field04";
            treeNode13.Name = "Node8";
            treeNode13.Text = "Field05";
            treeNode14.Name = "Node0";
            treeNode14.Text = "Frame";
            this.treeView2.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
            treeNode14});
            this.treeView2.Size = new System.Drawing.Size(133, 351);
            this.treeView2.TabIndex = 3;
            this.treeView2.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView2_AfterSelect);
            // 
            // g4XToolKitBindingSource1
            // 
            this.g4XToolKitBindingSource1.DataSource = typeof(WindowsFormsApplication1.G4XToolKit);
            // 
            // programBindingSource
            // 
            this.programBindingSource.DataSource = typeof(WindowsFormsApplication1.Program);
            // 
            // g4XToolKitBindingSource
            // 
            this.g4XToolKitBindingSource.DataSource = typeof(WindowsFormsApplication1.G4XToolKit);
            // 
            // G4XToolKit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(704, 574);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "G4XToolKit";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "G4X ToolKit";
            this.Load += new System.EventHandler(this.G4XToolKit_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabG4X.ResumeLayout(false);
            this.tabG4X.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.RTValues.ResumeLayout(false);
            this.RTValues.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nUDDelayMS)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDRepeatRTValue)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgFiledRTValueID)).EndInit();
            this.Tables.ResumeLayout(false);
            this.Tables.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSizeY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nUDSizeX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgTables)).EndInit();
            this.GPIOs.ResumeLayout(false);
            this.AnalogsInputs.ResumeLayout(false);
            this.AnalogsInputs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.g4XToolKitBindingSource1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.programBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.g4XToolKitBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabG4X;
        private System.Windows.Forms.TabPage RTValues;
        private System.Windows.Forms.ComboBox cBBaudrate;
        private System.Windows.Forms.ComboBox cBPortCOMM;
        private System.Windows.Forms.Button btConnectSerial;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage Tables;
        private System.Windows.Forms.TabPage GPIOs;
        private System.Windows.Forms.TabPage AnalogsInputs;
        private System.Windows.Forms.Button btGPIOs;
        private System.Windows.Forms.CheckedListBox checkBoxGPIO0;
        private System.Windows.Forms.Button btTables;
        private System.Windows.Forms.TreeView treeView2;
        private System.Windows.Forms.Button btRTValuesRead;
        private System.Windows.Forms.DataGridView dgTables;
        private System.Windows.Forms.NumericUpDown nUDSizeX;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.NumericUpDown nUDSizeY;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.CheckBox ckBoxChecksum;
        private System.Windows.Forms.RadioButton rbRead;
        private System.Windows.Forms.RadioButton rbWrite;
        private System.Windows.Forms.Label lRTValueRepeat;
        private System.Windows.Forms.NumericUpDown nUDRepeatRTValue;
        private System.Windows.Forms.DataGridView dgFiledRTValueID;
        private System.Windows.Forms.DataGridViewTextBoxColumn RTValueCol1;
        private System.Windows.Forms.DataGridViewTextBoxColumn RTValue;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button btAnalogRead;
        private System.Windows.Forms.BindingSource g4XToolKitBindingSource1;
        private System.Windows.Forms.BindingSource programBindingSource;
        private System.Windows.Forms.BindingSource g4XToolKitBindingSource;
        private System.Windows.Forms.NumericUpDown nUDDelayMS;
        private System.Windows.Forms.Button btGetConfig;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.CheckedListBox checkBoxGPIO1;
        private System.Windows.Forms.Button btWriteGpios;
    }
}

